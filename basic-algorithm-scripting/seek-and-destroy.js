/* You will be provided with an initial array
(the first argument in the destroyer function), 
followed by one or more arguments.Remove all 
elements from the initial array that are of 
the same value as these arguments.*/


function destroyer(arr) {
  let array = arguments['0'];
  let myFilter = Array.from(arguments);
  let target = myFilter.shift();
  let result = [];
  console.log(`myFilter: ${myFilter}`);
  console.log(`target: ${target}`);

  for (let i = 0; i < myFilter.length; i++) {
    let element = myFilter[i];
    let partialResult = target.filter(function(x){
      return (x != element);
    })
    target = partialResult;
    console.log(`partialResult: ${partialResult} filtering ${element}`);
    result = partialResult;
  }
  console.log(`result: ${result}`);

  return result;
}

let output = destroyer([1, 2, 3, 1, 2, 3, 5], 5, 3);
console.log(output);