function chunkArrayInGroups(arr, size) {
  // Break it up.
  var result = [];
  for(var i = 0; i < arr.length; i = i + size){
    result.push(arr.slice(i,i + size));
  }
  
  return result;
}

const array = ["a", "b", "c", "d"];
const size = 2;

console.log('INPUT: array: ',  array, 'size: ', size);
let result = chunkArrayInGroups(array, size);
console.log('OUTPUT:', result);
